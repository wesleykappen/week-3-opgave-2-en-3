from django.http import HttpResponse
from django.core import serializers

from django.db.models import Avg

from opdracht.power.models import Appliances, Productions

from django.utils import simplejson

def getAppliances(request):
    user_email = request.GET.get('user_email')

    if user_email not in [None, '']:
        appliances = Appliances.objects.filter(user__email_text=str(user_email))
        data = serializers.serialize('json', appliances)

        response = HttpResponse(data, content_type="application/json")
        response["Access-Control-Allow-Origin"] = "*"  
        response["Access-Control-Allow-Methods"] = "POST, GET, OPTIONS"  
        response["Access-Control-Max-Age"] = "1000"  
        response["Access-Control-Allow-Headers"] = "*"  

        return response
    else:
        data = {}
        data['result'] = 'error'
        data['message'] = 'something went wrong!'

        response = HttpResponse(simplejson.dumps(data), content_type="application/json")
        response["Access-Control-Allow-Origin"] = "*"  
        response["Access-Control-Allow-Methods"] = "POST, GET, OPTIONS"  
        response["Access-Control-Max-Age"] = "1000"  
        response["Access-Control-Allow-Headers"] = "*" 

        return response
        
def getProductions(request):
    appliance_id = request.GET.get('app_id')
    zipcode = request.GET.get('zipcode')
    type_soort = request.GET.get('type_soort')
    
    print type_soort

    if appliance_id not in [None, '']:
        appliance = Appliances.objects.get(id=appliance_id)

        container = dict()
        
        my_productions= Productions.objects.filter(appliance = appliance).aggregate(production_kw_0900=Avg('production_kw_0900'),
                                                                                     production_kw_1000=Avg('production_kw_1000'),
                                                                                     production_kw_1100=Avg('production_kw_1100'),
                                                                                     production_kw_1200=Avg('production_kw_1200'),
                                                                                     production_kw_1300=Avg('production_kw_1300'),
                                                                                     production_kw_1400=Avg('production_kw_1400'),
                                                                                     production_kw_1500=Avg('production_kw_1500'),
                                                                                     production_kw_1600=Avg('production_kw_1600'),
                                                                                     production_kw_1700=Avg('production_kw_1700'),
                                                                                     production_kw_1800=Avg('production_kw_1800'))
        
        if my_productions.get('production_kw_0900') in [None, '']:
            prod_dict = {}
            prod_dict['production_kw_0900'] = 0
            prod_dict['production_kw_1000'] = 0
            prod_dict['production_kw_1100'] = 0
            prod_dict['production_kw_1200'] = 0
            prod_dict['production_kw_1300'] = 0
            prod_dict['production_kw_1400'] = 0
            prod_dict['production_kw_1500'] = 0
            prod_dict['production_kw_1600'] = 0
            prod_dict['production_kw_1700'] = 0
            prod_dict['production_kw_1800'] = 0
            my_productions = prod_dict

        container['my_productions'] = my_productions

        container['other_productions'] = None
        if zipcode not in [None, '']:
            if type_soort == 'type':
                other_productions = Productions.objects.filter(appliance__type_nr = appliance.type_nr, appliance__user__zipcode_text = zipcode).aggregate(production_kw_0900=Avg('production_kw_0900'),
                                                                                 production_kw_1000=Avg('production_kw_1000'),
                                                                                 production_kw_1100=Avg('production_kw_1100'),
                                                                                 production_kw_1200=Avg('production_kw_1200'),
                                                                                 production_kw_1300=Avg('production_kw_1300'),
                                                                                 production_kw_1400=Avg('production_kw_1400'),
                                                                                 production_kw_1500=Avg('production_kw_1500'),
                                                                                 production_kw_1600=Avg('production_kw_1600'),
                                                                                 production_kw_1700=Avg('production_kw_1700'),
                                                                                 production_kw_1800=Avg('production_kw_1800'))
            else:
                other_productions = Productions.objects.filter(appliance__soort_text = appliance.soort_text, appliance__user__zipcode_text = zipcode).aggregate(production_kw_0900=Avg('production_kw_0900'),
                                                                                 production_kw_1000=Avg('production_kw_1000'),
                                                                                 production_kw_1100=Avg('production_kw_1100'),
                                                                                 production_kw_1200=Avg('production_kw_1200'),
                                                                                 production_kw_1300=Avg('production_kw_1300'),
                                                                                 production_kw_1400=Avg('production_kw_1400'),
                                                                                 production_kw_1500=Avg('production_kw_1500'),
                                                                                 production_kw_1600=Avg('production_kw_1600'),
                                                                                 production_kw_1700=Avg('production_kw_1700'),
                                                                                 production_kw_1800=Avg('production_kw_1800'))
        else:
            if type_soort == 'type':
                other_productions = Productions.objects.filter(appliance__type_nr = appliance.type_nr).aggregate(production_kw_0900=Avg('production_kw_0900'),
                                                                                 production_kw_1000=Avg('production_kw_1000'),
                                                                                 production_kw_1100=Avg('production_kw_1100'),
                                                                                 production_kw_1200=Avg('production_kw_1200'),
                                                                                 production_kw_1300=Avg('production_kw_1300'),
                                                                                 production_kw_1400=Avg('production_kw_1400'),
                                                                                 production_kw_1500=Avg('production_kw_1500'),
                                                                                 production_kw_1600=Avg('production_kw_1600'),
                                                                                 production_kw_1700=Avg('production_kw_1700'),
                                                                                 production_kw_1800=Avg('production_kw_1800'))
            else:
                other_productions = Productions.objects.filter(appliance__soort_text = appliance.soort_text).aggregate(production_kw_0900=Avg('production_kw_0900'),
                                                                                 production_kw_1000=Avg('production_kw_1000'),
                                                                                 production_kw_1100=Avg('production_kw_1100'),
                                                                                 production_kw_1200=Avg('production_kw_1200'),
                                                                                 production_kw_1300=Avg('production_kw_1300'),
                                                                                 production_kw_1400=Avg('production_kw_1400'),
                                                                                 production_kw_1500=Avg('production_kw_1500'),
                                                                                 production_kw_1600=Avg('production_kw_1600'),
                                                                                 production_kw_1700=Avg('production_kw_1700'),
                                                                                 production_kw_1800=Avg('production_kw_1800'))
        
        if other_productions.get('production_kw_0900') in [None, '']:
            prod_dict = {}
            prod_dict['production_kw_0900'] = 0
            prod_dict['production_kw_1000'] = 0
            prod_dict['production_kw_1100'] = 0
            prod_dict['production_kw_1200'] = 0
            prod_dict['production_kw_1300'] = 0
            prod_dict['production_kw_1400'] = 0
            prod_dict['production_kw_1500'] = 0
            prod_dict['production_kw_1600'] = 0
            prod_dict['production_kw_1700'] = 0
            prod_dict['production_kw_1800'] = 0
            other_productions = prod_dict
        else:
            prod_dict = {}
            prod_dict['production_kw_0900'] = round(other_productions.get('production_kw_0900'), 2)
            prod_dict['production_kw_1000'] = round(other_productions.get('production_kw_1000'), 2)
            prod_dict['production_kw_1100'] = round(other_productions.get('production_kw_1100'), 2)
            prod_dict['production_kw_1200'] = round(other_productions.get('production_kw_1200'), 2)
            prod_dict['production_kw_1300'] = round(other_productions.get('production_kw_1300'), 2)
            prod_dict['production_kw_1400'] = round(other_productions.get('production_kw_1400'), 2)
            prod_dict['production_kw_1500'] = round(other_productions.get('production_kw_1500'), 2)
            prod_dict['production_kw_1600'] = round(other_productions.get('production_kw_1600'), 2)
            prod_dict['production_kw_1700'] = round(other_productions.get('production_kw_1700'), 2)
            prod_dict['production_kw_1800'] = round(other_productions.get('production_kw_1800'), 2)
            other_productions = prod_dict
            
        container['other_productions'] = other_productions
            
        data = simplejson.dumps(container)

        response = HttpResponse(data, content_type="application/json")
        response["Access-Control-Allow-Origin"] = "*"  
        response["Access-Control-Allow-Methods"] = "POST, GET, OPTIONS"  
        response["Access-Control-Max-Age"] = "1000"  
        response["Access-Control-Allow-Headers"] = "*"  

        return response