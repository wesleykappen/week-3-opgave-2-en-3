$(document).ready(function(){
		
	var email = $('#invoer').find('input[name="email"]').val();
	
	$('#appliancesList').children('option:not(:first)').remove();
	
	$.ajax({
		crossDomain: true,	
		dataType: 'json',
		url: 'http://127.0.0.1:8000/api/appliances/get',
		type: 'GET',
		data: {
			'user_email' : email
		},
		headers : {Accept : "application/json"},
		success: function(data){
			if(data.length > 0){
				$.each(data, function(index, value){
					$('#appliancesList').append($('<option>', { 
				        value: value.pk,
				        text : value.fields.soort_text + ' - ' + value.fields.type_nr + ' - ' + value.fields.brand_text 
				    }));
				});
				
				console.log(JSON.stringify(data, null, 2));
				
				$('#invoer').show();
			}
			else {
				$('#leeg').show();
			}
		},
		error: function(xhr,textStatus,err)
		{
		    console.log("readyState: " + xhr.readyState);
		    console.log("responseText: "+ xhr.responseText);
		    console.log("status: " + xhr.status);
		    console.log("text status: " + textStatus);
		    console.log("error: " + err);
		},
		jsonp: true
	});
	
	$('#toon-grafiek').click(function(){
		var app_id = $('#appliancesList').val();
		var app_text = $("#appliancesList option[value='" + app_id + "']").text()
		var zipcode = $('#invoer').find('input[name="zipcode"]').val();
		var typeSoort = $('#typeSoort').val();
		
		console.log("typeSoort: " + typeSoort);
		
		if (app_id === "")
		{
			$('#errorDiv').show();
			return false;
		}
		
		$.ajax({
			crossDomain: true,	
			dataType: 'json',
			url: 'http://127.0.0.1:8000/api/productions/get',
			type: 'GET',
			data: {
				'app_id' : app_id,
				'zipcode' : zipcode,
				'type_soort' : typeSoort
			},
			headers : {Accept : "application/json"},
			success: function(data){
				console.log(JSON.stringify(data, null, 2));
				var my_jsondata = data.my_productions;
				var other_jsondata = data.other_productions;
				
				$('#dataDiv').show();
				$('#errorDiv').hide();
				
				var labelsLong = [[my_jsondata.production_kw_0900, 
				                   my_jsondata.production_kw_1000, 
				                   my_jsondata.production_kw_1100, 
				                   my_jsondata.production_kw_1200, 
				                   my_jsondata.production_kw_1300, 
				                   my_jsondata.production_kw_1400, 
				                   my_jsondata.production_kw_1500, 
				                   my_jsondata.production_kw_1600, 
				                   my_jsondata.production_kw_1700, 
				                   my_jsondata.production_kw_1800],[other_jsondata.production_kw_0900, 
				                                                    other_jsondata.production_kw_1000, 
				                                                    other_jsondata.production_kw_1100, 
				                                                    other_jsondata.production_kw_1200, 
				                                                    other_jsondata.production_kw_1300, 
				                                                    other_jsondata.production_kw_1400, 
				                                                    other_jsondata.production_kw_1500, 
				                                                    other_jsondata.production_kw_1600, 
				                                                    other_jsondata.production_kw_1700, 
				                                                    other_jsondata.production_kw_1800]];
				
				$('#titel').html('<h2>' + app_text + '</h2>');
				$('#holder').children().remove();
				var r = Raphael('holder');
		        var labels = ['09:00','10:00','11:00','12:00','13:00','14:00','15:00','16:00','17:00','18:00'],
		        	labelsLong = labelsLong,
		        	data = labelsLong;
			    r.raphalytics(data,labels,labelsLong,{'width':800,'height':400,'gridtype':'full_grid','fill':false,'color':['blue','red'], 'y_labels_number':2, 'y_labels_position':'outside'});
			},
			error: function(xhr,textStatus,err)
			{
			    console.log("readyState: " + xhr.readyState);
			    console.log("responseText: "+ xhr.responseText);
			    console.log("status: " + xhr.status);
			    console.log("text status: " + textStatus);
			    console.log("error: " + err);
			},
			jsonp: true
		});
		
		return false;
	});
});