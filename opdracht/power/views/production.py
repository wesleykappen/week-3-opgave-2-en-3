from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render

from opdracht.power.models import User, Appliances, Productions

def production_add(request):
	app_id = request.GET.get('id')
	UserAccountId = request.session.get('UserAccountId', None)
	provider = request.session.get('provider', None)
	
	if UserAccountId is not None:
		
		account = User.objects.get(id=UserAccountId)
		
		if app_id in [None, ''] :
			return render(request, 'production/production_add.html', {
				'title' : 'Opbrengst invoeren',
				'result' : 0,
				'result_msg' : 'U heeft geen apparaat geselecteerd.',
				'User' : account,
				'provider' : provider
			})
			
		try:
			appliance = Appliances.objects.get(id=app_id)
		except Exception as e:
			return render(request, 'production/production_add.html', {
				'title' : 'Opbrengst invoeren',
				'result' : 0,
				'result_msg' : 'Er bestaat geen apparaat met id: ' + app_id,
				'User' : account,
				'provider' : provider
			})
		
		if appliance.user.id != UserAccountId :
			return render(request, 'production/production_add.html', {
				'title' : 'Opbrengst invoeren',
				'result' : 0,
				'result_msg' : 'Dit apparaat is niet van u.',
				'User' : account,
				'provider' : provider
			})
		
		try:
			production = Productions.objects.get(appliance=appliance)
		except Exception as e:
			production = Productions(production_kw_0900 = 0, 
						production_kwh_0900 = 0, 
						production_kw_1000 = 0,
						production_kwh_1000 = 0,
						production_kw_1100 = 0,
						production_kwh_1100 = 0,
						production_kw_1200 = 0,
						production_kwh_1200 = 0,
						production_kw_1300 = 0,
						production_kwh_1300 = 0,
						production_kw_1400 = 0,
						production_kwh_1400 = 0,
						production_kw_1500 = 0,
						production_kwh_1500 = 0, 
						production_kw_1600 = 0,
						production_kwh_1600 = 0, 
						production_kw_1700 = 0,
						production_kwh_1700 = 0, 
						production_kw_1800 = 0,
						production_kwh_1800 = 0,  
						appliance = appliance)
			
		if request.method == 'POST':
			prod_kw_0900 = request.POST.get('prod_kw_0900', '').replace(',','.')
			prod_kw_1000 = request.POST.get('prod_kw_1000', '').replace(',','.')
			prod_kw_1100 = request.POST.get('prod_kw_1100', '').replace(',','.')
			prod_kw_1200 = request.POST.get('prod_kw_1200', '').replace(',','.')
			prod_kw_1300 = request.POST.get('prod_kw_1300', '').replace(',','.')
			prod_kw_1400 = request.POST.get('prod_kw_1400', '').replace(',','.')
			prod_kw_1500 = request.POST.get('prod_kw_1500', '').replace(',','.')
			prod_kw_1600 = request.POST.get('prod_kw_1600', '').replace(',','.')
			prod_kw_1700 = request.POST.get('prod_kw_1700', '').replace(',','.')
			prod_kw_1800 = request.POST.get('prod_kw_1800', '').replace(',','.')
			
			production = Productions(production_kw_0900 = prod_kw_0900, 
						production_kwh_0900 = production.production_kwh_0900, 
						production_kw_1000 = prod_kw_1000,
						production_kwh_1000 = production.production_kwh_1000,
						production_kw_1100 = prod_kw_1100,
						production_kwh_1100 = production.production_kwh_1100,
						production_kw_1200 = prod_kw_1200,
						production_kwh_1200 = production.production_kwh_1200,
						production_kw_1300 = prod_kw_1300,
						production_kwh_1300 = production.production_kwh_1300,
						production_kw_1400 = prod_kw_1400,
						production_kwh_1400 = production.production_kwh_1400,
						production_kw_1500 = prod_kw_1500,
						production_kwh_1500 = production.production_kwh_1500, 
						production_kw_1600 = prod_kw_1600,
						production_kwh_1600 = production.production_kwh_1600, 
						production_kw_1700 = prod_kw_1700,
						production_kwh_1700 = production.production_kwh_1700, 
						production_kw_1800 = prod_kw_1800,
						production_kwh_1800 = production.production_kwh_1800,  
						appliance = appliance)

			if (prod_kw_0900 in [None, ''] or prod_kw_1000 in [None, ''] or prod_kw_1100 in [None, ''] or prod_kw_1200 in [None, ''] or prod_kw_1300 in [None, ''] or 
			prod_kw_1400 in [None, ''] or prod_kw_1500 in [None, ''] or prod_kw_1600 in [None, ''] or prod_kw_1700 in [None, ''] or prod_kw_1800 in [None, '']):
				return render(request, 'production/production_add.html', {
					'title' : 'Opbrengst invoeren',
					'result' : 0,
					'result_msg' : 'kW mag niet leeg zijn.',
					'app_id' : app_id, 
					'User' : account, 
					'provider' : provider,
					'Appliance' : appliance,
					'Production' : production
				})
				
			if (prod_kw_0900.replace('.','',1).isdigit() != True or prod_kw_1000.replace('.','',1).isdigit() != True or 
			prod_kw_1100.replace('.','',1).isdigit() != True or prod_kw_1200.replace('.','',1).isdigit() != True or 
			prod_kw_1300.replace('.','',1).isdigit() != True or prod_kw_1400.replace('.','',1).isdigit() != True or 
			prod_kw_1500.replace('.','',1).isdigit() != True or prod_kw_1600.replace('.','',1).isdigit() != True or 
			prod_kw_1700.replace('.','',1).isdigit() != True or prod_kw_1800.replace('.','',1).isdigit() != True):
				return render(request, 'production/production_add.html', {
					'title' : 'Opbrengst invoeren',
					'result' : 0,
					'result_msg' : 'kW moet een getal zijn.',
					'app_id' : app_id, 
					'User' : account, 
					'provider' : provider,
					'Appliance' : appliance,
					'Production' : production
				})
				
			if (prod_kw_0900 < 0 or prod_kw_1000 < 0 or prod_kw_1100 < 0 or prod_kw_1200 <= 0 or prod_kw_1300 <= 0 
			 or prod_kw_1400 < 0 or prod_kw_1500 < 0 or prod_kw_1600 < 0 or prod_kw_1700 < 0 or prod_kw_1800 <= 0):
				return render(request, 'production/production_add.html', {
					'title' : 'Opbrengst invoeren',
					'result' : 0,
					'result_msg' : 'kW moet een positief getal zijn.',
					'app_id' : app_id, 
					'User' : account, 
					'provider' : provider,
					'Appliance' : appliance,
					'Production' : production
				})
			
			prod_kwh_0900 = float(prod_kw_0900)
			prod_kwh_1000 = prod_kwh_0900 + float(prod_kw_1000)
			prod_kwh_1100 = prod_kwh_1000 + float(prod_kw_1100)
			prod_kwh_1200 = prod_kwh_1100 + float(prod_kw_1200)
			prod_kwh_1300 = prod_kwh_1200 + float(prod_kw_1300)
			prod_kwh_1400 = prod_kwh_1300 + float(prod_kw_1400)
			prod_kwh_1500 = prod_kwh_1400 + float(prod_kw_1500)
			prod_kwh_1600 = prod_kwh_1500 + float(prod_kw_1600)
			prod_kwh_1700 = prod_kwh_1600 + float(prod_kw_1700)
			prod_kwh_1800 = prod_kwh_1700 + float(prod_kw_1800)
			
			production = Productions(production_kw_0900 = prod_kw_0900, 
						production_kwh_0900 = prod_kwh_0900, 
						production_kw_1000 = prod_kw_1000,
						production_kwh_1000 = prod_kwh_1000,
						production_kw_1100 = prod_kw_1100,
						production_kwh_1100 = prod_kwh_1100,
						production_kw_1200 = prod_kw_1200,
						production_kwh_1200 = prod_kwh_1200,
						production_kw_1300 = prod_kw_1300,
						production_kwh_1300 = prod_kwh_1300,
						production_kw_1400 = prod_kw_1400,
						production_kwh_1400 = prod_kwh_1400,
						production_kw_1500 = prod_kw_1500,
						production_kwh_1500 = prod_kwh_1500, 
						production_kw_1600 = prod_kw_1600,
						production_kwh_1600 = prod_kwh_1600, 
						production_kw_1700 = prod_kw_1700,
						production_kwh_1700 = prod_kwh_1700, 
						production_kw_1800 = prod_kw_1800,
						production_kwh_1800 = prod_kwh_1800,  
						appliance = appliance)
				
			try:
				checkrecord = Productions.objects.filter(appliance=appliance)
				
				if (checkrecord.count() > 0):
					
					print prod_kw_0900
					print prod_kwh_0900
					print prod_kw_1000
					print prod_kwh_1000
					print prod_kw_1100
					print prod_kwh_1100
					print prod_kw_1200
					print prod_kwh_1200
					print prod_kw_1300
					print prod_kwh_1300
					print prod_kw_1400
					print prod_kwh_1400
					print prod_kw_1500
					print prod_kwh_1500
					print prod_kw_1600
					print prod_kwh_1600
					print prod_kw_1700
					print prod_kwh_1700
					print prod_kw_1800
					print prod_kwh_1800
					
					Productions.objects.filter(appliance=appliance).update(production_kw_0900 = prod_kw_0900, production_kwh_0900 = prod_kwh_0900, production_kw_1000 = prod_kw_1000,
																		production_kwh_1000 = prod_kwh_1000, production_kw_1100 = prod_kw_1100, production_kwh_1100 = prod_kwh_1100,
																		production_kw_1200 = prod_kw_1200, production_kwh_1200 = prod_kwh_1200,	production_kw_1300 = prod_kw_1300,
																		production_kwh_1300 = prod_kwh_1300, production_kw_1400 = prod_kw_1400, production_kwh_1400 = prod_kwh_1400,
																		production_kw_1500 = prod_kw_1500, production_kwh_1500 = prod_kwh_1500, production_kw_1600 = prod_kw_1600,
																		production_kwh_1600 = prod_kwh_1600, production_kw_1700 = prod_kw_1700, production_kwh_1700 = prod_kwh_1700,
																		production_kw_1800 = prod_kw_1800, production_kwh_1800 = prod_kwh_1800)
					
					return render(request, 'production/production_add.html', {
						'title' : 'Opbrengst invoeren', 
						'result' : 1,
						'result_msg' : 'Uw opbrengst is met succes aangepast.',
						'app_id' : app_id, 
						'User' : account, 
						'provider' : provider,
						'Appliance' : appliance,
						'Production' : production
					})
				
				production.save()
				return render(request, 'production/production_add.html', {
					'title' : 'Opbrengst invoeren', 
					'result' : 1,
					'result_msg' : 'Uw opbrengst is met succes toegevoegd.',
					'app_id' : app_id, 
					'User' : account, 
					'provider' : provider,
					'Appliance' : appliance,
					'Production' : production
				})
			except Exception as e:
				return HttpResponse(e.message)	
		else:
			return render(request, 'production/production_add.html', {
				'title' : 'Opbrengst invoeren',
				'app_id' : app_id, 
				'User' : account, 
				'provider' : provider,
				'Appliance' : appliance,
				'Production' : production
			})
	else:
		return HttpResponseRedirect('/login?referer=production/add?id=' + app_id)
	