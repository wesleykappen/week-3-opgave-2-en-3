from django.http import HttpResponse
from django.shortcuts import render
import urllib, urllib2, json
from opdracht.power.models import User

def google(request):
    facebook_client_id = '524708244281381'
    facebook_redirect_uri = 'http://localhost:8000/facebook'
    facebook_scope = 'email'
    google_client_id = '202665462009.apps.googleusercontent.com'
    google_redirect_uri = 'http://localhost:8000/google'
    google_scope = 'email'
    
    CLIENT_SECRET = 'KWMGoSA5uloqb8YhKABZNnYy'
    CODE = request.GET.get('code')
    
    if not CODE in [None, '']:
        details = getAccessTokenDetails(google_client_id, CLIENT_SECRET, google_redirect_uri, CODE)
        
        if not details["access_token"] is None:
            tokenInfo = getTokenInfo(details["id_token"])
            userInfo = getUserDetails(details["access_token"])
            
            google_name = userInfo['displayName']
            google_email = tokenInfo['email']
            google_id = userInfo['id']
            google_image = userInfo['image']['url']
            
            account = getUser(google_email)
            
            if not account is None:
                # Gebruikers account bestaat
                if account.google_id in [None, '']:
                    # Account heeft nog geen google id, voeg deze toe 
                    User.objects.filter(id=account.id).update(google_id = google_id, google_image = google_image)
                    result_msg = 'U heeft al een account op deze site, uw Google gegevens zijn nu gekoppeld aan dit account. U wordt doorverwezen.'
                else:
                    # Account heeft al een google id 
                    result_msg = 'U bent ingelogd met uw Google account. U wordt doorverwezen.'
            else:
                # Gebruikers account bestaat niet, maak deze aan
                account = User(name_text = google_name, 
                    email_text = google_email,
                    google_id = google_id,
                    google_image = google_image)
                try:
                    account.save()
                except Exception as e:
                    return HttpResponse(e.message)
                result_msg = 'U bent nieuw op deze site, er is een account voor u aangemaakt. U wordt doorverwezen.'
            
            provider = 'google'
            request.session['UserAccountId'] = account.id
            request.session['provider'] = provider
            
            return render(request, 'google/google.html', {
                'title' : 'Google',
                'result' : 1,
                'result_msg' : result_msg,
                'User' : account,
                'provider' : provider
            })
    
    return render(request, 'login.html', {
            'title' : 'Login',
            'facebook_client_id' : facebook_client_id,
            'facebook_redirect_uri' : facebook_redirect_uri,
            'facebook_scope' : facebook_scope,
            'google_client_id' : google_client_id,
            'google_redirect_uri' : google_redirect_uri,
            'google_scope' : google_scope
        })

def parse_str(string):
    nvps = {};
    lijst = string.rsplit("&")
    for el in lijst:
        nvplist = el.rsplit("=")
        nvps[nvplist[0]] = nvplist[1]
    return nvps
 
def getAccessTokenDetails(client_id, client_secret, redirect_uri, code):
    lijst = {}
    
    url =  "https://accounts.google.com/o/oauth2/token"
    request = urllib2.Request(url)
    query_args = {'code': code, 'client_id': client_id, 'client_secret': client_secret, 'redirect_uri': redirect_uri, 'grant_type': 'authorization_code'}
    request.add_data(urllib.urlencode(query_args))
    request.add_header("Content-type", "application/x-www-form-urlencoded")
    try: 
        response = urllib2.urlopen(request)
        string = response.read()
        lijst = json.loads(string)
    except Exception, e:
        print e
    return lijst

def getTokenInfo(id_token):
    lijst = {}
    
    url = "https://www.googleapis.com/oauth2/v1/tokeninfo?id_token=" + id_token;
    req = urllib2.Request(url)
    try: 
        response= urllib2.urlopen(req)
        string = response.read()
        lijst = json.loads(string)
    except Exception, e:
        print e
    return lijst
 
def getUserDetails(access_token):
    lijst = {}
    
    url = "https://www.googleapis.com/plus/v1/people/me?access_token=" + access_token;
    req = urllib2.Request(url)
    try: 
        response= urllib2.urlopen(req)
        string = response.read()
        lijst = json.loads(string)
    except Exception, e:
        print e
    return lijst

def getUser(email):
    try:
        account = User.objects.get(email_text = email)
        return account
    except User.DoesNotExist:
        return None
