from django.http import HttpResponse
from django.shortcuts import render
import urllib2, json
from opdracht.power.models import User

def facebook(request):
    facebook_client_id = '524708244281381'
    facebook_redirect_uri = 'http://localhost:8000/facebook'
    facebook_scope = 'email'
    google_client_id = '202665462009.apps.googleusercontent.com'
    google_redirect_uri = 'http://localhost:8000/google'
    google_scope = 'email'

    APP_SECRET = '3fa7c52f0eb330ad003f966f4fe384d6'
    CODE = request.GET.get('code')
    
    if not CODE in [None, '']:
        details = getAccessTokenDetails(facebook_client_id, APP_SECRET, facebook_redirect_uri, CODE)
    
        if not details["access_token"] is None:
            userInfo = getUserDetails(details["access_token"])
            
            facebook_name = userInfo['name']
            facebook_email = userInfo['email']
            facebook_id = userInfo['id']
            facebook_image = 'https://graph.facebook.com/' + facebook_id + '/picture'
            
            account = getUser(facebook_email)
            
            if not account is None:
                # Gebruikers account bestaat
                if account.facebook_id in [None, '']:
                    # Account heeft nog geen facbook id, voeg deze toe 
                    User.objects.filter(id=account.id).update(facebook_id = facebook_id, facebook_image = facebook_image)
                    result_msg = 'U heeft al een account op deze site, uw Facebook gegevens zijn nu gekoppeld aan dit account. U wordt doorverwezen.'
                else:
                    # Account heeft al een facbook id 
                    result_msg = 'U bent ingelogd met uw Facebook account. U wordt doorverwezen.'
            else:
                # Gebruikers account bestaat niet, maak deze aan
                account = User(name_text = facebook_name, 
                    email_text = facebook_email,
                    facebook_id = facebook_id,
                    facebook_image = facebook_image)
                try:
                    account.save()
                except Exception as e:
                    return HttpResponse(e.message)
                result_msg = 'U bent nieuw op deze site, er is een account voor u aangemaakt. U wordt doorverwezen.'
            
            provider = 'facebook'
            request.session['UserAccountId'] = account.id
            request.session['provider'] = provider
            
            return render(request, 'facebook/facebook.html', {
                'title' : 'Facebook',
                'result' : 1,
                'result_msg' : result_msg,
                'User' : account,
                'provider' : provider
            })
    
    return render(request, 'login.html', {
            'title' : 'Login',
            'facebook_client_id' : facebook_client_id,
            'facebook_redirect_uri' : facebook_redirect_uri,
            'facebook_scope' : facebook_scope,
            'google_client_id' : google_client_id,
            'google_redirect_uri' : google_redirect_uri,
            'google_scope' : google_scope
        })

def parse_str(string):
    nvps = {};
    lijst = string.rsplit("&")
    for el in lijst:
        nvplist = el.rsplit("=")
        nvps[nvplist[0]] = nvplist[1]
    return nvps
 
def getAccessTokenDetails(client_id, app_secret, redirect_uri, code):
    lijst = {}
    
    url =  "https://graph.facebook.com/oauth/access_token?client_id=" + client_id + "&redirect_uri=" + redirect_uri + "&client_secret=" + app_secret + "&code=" + code;
    request = urllib2.Request(url)
    try: 
        response= urllib2.urlopen(request)
        string = response.read()
        lijst = parse_str(string)
 
    except Exception, e:
        print e
    return lijst
 
def getUserDetails(access_token):
    lijst = {}
    
    url = "https://graph.facebook.com/me?access_token=" + access_token;
    request = urllib2.Request(url)
    try: 
        response= urllib2.urlopen(request)
        string = response.read()
        lijst = json.loads(string)
    except Exception, e:
        print e
 
    return lijst

def getUser(email):
    try:
        account = User.objects.get(email_text = email)
        return account
    except User.DoesNotExist:
        return None
