from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render

from opdracht.power.models import User, Appliances

def appliance_add(request):
	UserAccountId = request.session.get('UserAccountId', None)
	provider = request.session.get('provider', None)
	
	if UserAccountId is not None:
		account = User.objects.get(id=UserAccountId)
		
		if request.method == 'POST':
			soort = request.POST.get('app_soort', '')
			brand = request.POST.get('app_brand', '')
			typenr = request.POST.get('app_nr', '')
			description = request.POST.get('app_description', '')
			
			appliances = Appliances(soort_text = soort,
						desc_text = description, 
						brand_text = brand, 
						type_nr = typenr,
						user = account)
			
			if (soort in [None, '']):
				return render(request, 'appliance/appliance_add.html', {
					'title' : 'Apparaat toevoegen',
					'result' : 0,
					'result_msg' : 'Soort mag niet leeg zijn.',
					'User' : account, 
					'provider' : provider,
					'Appliances' : appliances
				})
			
			if (brand in [None, '']):
				return render(request, 'appliance/appliance_add.html', {
					'title' : 'Apparaat toevoegen',
					'result' : 0,
					'result_msg' : 'Merk mag niet leeg zijn.',
					'User' : account, 
					'provider' : provider,
					'Appliances' : appliances
				})
				
			if (typenr in [None, '']):
				return render(request, 'appliance/appliance_add.html', {
					'title' : 'Apparaat toevoegen',
					'result' : 0,
					'result_msg' : 'Type nr. mag niet leeg zijn.',
					'User' : account, 
					'provider' : provider,
					'Appliances' : appliances
				})
				
			if (description in [None, '']):
				return render(request, 'appliance/appliance_add.html', {
					'title' : 'Apparaat toevoegen',
					'result' : 0,
					'result_msg' : 'Omschrijving mag niet leeg zijn.',
					'User' : account, 
					'provider' : provider,
					'Appliances' : appliances
				})
				
			try:
				appliances.save()
				return render(request, 'appliance/appliance_add.html', {
					'title' : 'Apparaat toevoegen',
					'result' : 1,
					'result_msg' : 'Uw apparaat is met succes toegevoegd.', 
					'User' : account, 
					'provider' : provider,
					'Appliances' : appliances
				})
			except Exception as e:
				return HttpResponse(e.message)	
		else:
			return render(request, 'appliance/appliance_add.html', {
				'title' : 'Apparaat toevoegen',
				'User' : account,
				'provider' : provider
			})
	else:
		return HttpResponseRedirect('/login?referer=appliance/add')

def appliance_edit(request):
	app_id = request.GET.get('id')
	UserAccountId = request.session.get('UserAccountId', None)
	provider = request.session.get('provider', None)
	
	if UserAccountId is not None:
		
		account = User.objects.get(id=UserAccountId)
		
		if app_id in [None, ''] :
			return render(request, 'appliance/appliance_edit.html', {
				'title' : 'Apparaat wijzigen',
				'result' : 0,
				'result_msg' : 'U heeft geen apparaat geselecteerd.',
				'User' : account,
				'provider' : provider
			})
			
		try:
			appliances = Appliances.objects.get(id=app_id)
		except Exception as e:
			return render(request, 'appliance/appliance_edit.html', {
				'title' : 'Apparaat wijzigen',
				'result' : 0,
				'result_msg' : 'Er bestaat geen apparaat met id: ' + app_id,
				'User' : account,
				'provider' : provider
			})
		
		if appliances.user.id != UserAccountId :
			return render(request, 'appliance/appliance_edit.html', {
				'title' : 'Apparaat wijzigen',
				'result' : 0,
				'result_msg' : 'Dit apparaat is niet van u.',
				'User' : account,
				'provider' : provider
			})
		
		if request.method == 'POST':
			soort = request.POST.get('app_soort', '')
			brand = request.POST.get('app_brand', '')
			typenr = request.POST.get('app_nr', '')
			description = request.POST.get('app_description', '')
			
			appliances = Appliances(desc_text = description, 
						brand_text = brand, 
						type_nr = typenr)

			if (soort in [None, '']):
				return render(request, 'appliance/appliance_edit.html', {
					'title' : 'Apparaat wijzigen',
					'result' : 0,
					'result_msg' : 'Soort mag niet leeg zijn.',
					'app_id' : app_id, 
					'User' : account, 
					'provider' : provider,
					'Appliances' : appliances
				})
				
			if (brand in [None, '']):
				return render(request, 'appliance/appliance_edit.html', {
					'title' : 'Apparaat wijzigen',
					'result' : 0,
					'result_msg' : 'Merk mag niet leeg zijn.',
					'app_id' : app_id, 
					'User' : account, 
					'provider' : provider,
					'Appliances' : appliances
				})
				
			if (typenr in [None, '']):
				return render(request, 'appliance/appliance_edit.html', {
					'title' : 'Apparaat wijzigen',
					'result' : 0,
					'result_msg' : 'Type nr. mag niet leeg zijn.',
					'app_id' : app_id, 
					'User' : account, 
					'provider' : provider,
					'Appliances' : appliances
				})
				
			if (description in [None, '']):
				return render(request, 'appliance/appliance_edit.html', {
					'title' : 'Apparaat wijzigen',
					'result' : 0,
					'result_msg' : 'Omschrijving mag niet leeg zijn.',
					'app_id' : app_id, 
					'User' : account, 
					'provider' : provider,
					'Appliances' : appliances
				})
				
			try:
				Appliances.objects.filter(id=app_id).update(brand_text = brand, type_nr = typenr, desc_text = description)
				return render(request, 'appliance/appliance_edit.html', {
					'title' : 'Apparaat wijzigen',
					'result' : 1,
					'result_msg' : 'Uw apparaat is met succes gewijzigd.',
					'app_id' : app_id, 
					'User' : account, 
					'provider' : provider,
					'Appliances' : appliances
				})
			except Exception as e:
				return HttpResponse(e.message)	
		else:
			return render(request, 'appliance/appliance_edit.html', {
				'title' : 'Apparaat wijzigen', 
				'app_id' : app_id, 
				'User' : account, 
				'provider' : provider,
				'Appliances' : appliances
			})
	else:
		return HttpResponseRedirect('/login?referer=appliance/edit?id=' + app_id)
