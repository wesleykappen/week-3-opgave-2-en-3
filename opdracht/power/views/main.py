from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render
from django.core.validators import email_re
from opdracht.power.models import User, Appliances, Productions

import hashlib

def index(request):
	UserAccount = request.session.get('UserAccountId', None)
	provider = request.session.get('provider', None)
	account = None
	if UserAccount is not None:
		account = User.objects.get(id=UserAccount)
	return render(request, 'index.html', {
		'title' : 'Home', 
		'User' : account,
		'provider' : provider
	})
	
def privacypolicy(request):
	UserAccount = request.session.get('UserAccountId', None)
	provider = request.session.get('provider', None)
	account = None
	if UserAccount is not None:
		account = User.objects.get(id=UserAccount)
	return render(request, 'privacy-policy.html', {
		'title' : 'Privacy Policy', 
		'User' : account,
		'provider' : provider
	})

def login(request):
	referer = request.GET.get('referer')
	facebook_client_id = '524708244281381'
	facebook_redirect_uri = 'http://localhost:8000/facebook'
	facebook_scope = 'email'
	google_client_id = '202665462009.apps.googleusercontent.com'
	google_redirect_uri = 'http://localhost:8000/google'
	google_scope = 'email'

	if request.method == 'POST':
		email = request.POST.get('username', None)
		userpass = str(hashlib.md5(request.POST.get('password', None)).hexdigest())
		
		checkrecord = User.objects.filter(email_text = email, password = userpass)
		
		if checkrecord.count() == 1:
			request.session['UserAccountId'] = checkrecord[0].id
			request.session['provider'] = 'local'
			return render(request, 'login.html', {
				'title' : 'Login',
				'result' : 1,
				'result_msg' : 'U bent ingelogd. U wordt doorverwezen.', 
				'referer' : referer
			})
		else:
			return render(request, 'login.html', {
				'title' : 'Login',
				'result' : 0,
				'result_msg' : 'Onjuiste combinatie e-mailadres/wachtwoord.',
				'emailadres' : email,
				'referer' : referer
			})
	else:
		return render(request, 'login.html', {
				'title' : 'Login',
				'referer' : referer,
				'facebook_client_id' : facebook_client_id,
                'facebook_redirect_uri' : facebook_redirect_uri,
                'facebook_scope' : facebook_scope,
                'google_client_id' : google_client_id,
                'google_redirect_uri' : google_redirect_uri,
                'google_scope' : google_scope
			})

def logout(request):
	if request.method == 'GET':
		request.session.flush()
		return HttpResponseRedirect('/') 
	else:
		return HttpResponseRedirect('/')

def register(request):
	if request.method == 'POST':
		name = request.POST.get('name', '')
		email1 = request.POST.get('email1', '')
		email2 = request.POST.get('email2', '')
		password1 = request.POST.get('password1', '')
		password2 = request.POST.get('password2', '')
		userpass = str(hashlib.md5(password1).hexdigest())
		address = request.POST.get('address', '')
		zipcode = request.POST.get('zipcode', '')
		city = request.POST.get('city', '')
		phone = request.POST.get('phone', '')
		famsize = request.POST.get('famsize', '')
		
		account = User(name_text = name, 
					email_text = email1,
					password = userpass,
					address_text = address, 
					zipcode_text = zipcode, 
					city_text = city,
					phone_text = phone,
					familysize = famsize)
		
		if (name in [None, '']):
			return render(request, 'register.html', {
				'title' : 'Registreren',
				'result' : 0,
				'result_msg' : 'Naam mag niet leeg zijn.',
				'User' : account,
				'email2' : email2
			})
		if (email1 in [None, '']):
			return render(request, 'register.html', {
				'title' : 'Registreren',
				'result' : 0,
				'result_msg' : 'Email adres mag niet leeg zijn.',
				'User' : account,
				'email2' : email2
			})
		if not (email_re.match(email1)):
			return render(request, 'register.html', {
				'title' : 'Registreren',
				'result' : 0,
				'result_msg' : 'U heeft geen geldig e-mailadres ingevoerd.',
				'User' : account,
				'email2' : email2
			})
		if (email1 != email2):
			return render(request, 'register.html', {
				'title' : 'Registreren',
				'result' : 0,
				'result_msg' : 'De e-mailadressen komen niet overeen.',
				'User' : account,
				'email2' : email2
			})
		if (password1 in [None, '']):
			return render(request, 'register.html', {
				'title' : 'Registreren',
				'result' : 0,
				'result_msg' : 'Wachtwoord mag niet leeg zijn.',
				'User' : account,
				'email2' : email2
			})
		if (password1 != password2):
			return render(request, 'register.html', {
				'title' : 'Registreren',
				'result' : 0,
				'result_msg' : 'De wachtwoorden komen niet overeen.',
				'User' : account,
				'email2' : email2
			})
		if (zipcode in [None, '']):
			return render(request, 'register.html', {
				'title' : 'Registreren',
				'result' : 0,
				'result_msg' : 'Postcode mag niet leeg zijn.',
				'User' : account,
				'email2' : email2
			})
		if (len(zipcode) < 4 or zipcode[:4].isdigit() != True):
			return render(request, 'register.html', {
				'title' : 'Registreren',
				'result' : 0,
				'result_msg' : 'Postcode moet minimaal uit 4 getallen bestaan.',
				'User' : account,
				'email2' : email2
			})
		if (famsize in [None, '']):
			return render(request, 'register.html', {
				'title' : 'Registreren',
				'result' : 0,
				'result_msg' : 'Aantal personen in gezin mag niet leeg zijn.',
				'User' : account,
				'email2' : email2
			})
	
		if (famsize.isdigit() != True):
			return render(request, 'register.html', {
				'title' : 'Registreren',
				'result' : 0,
				'result_msg' : 'Aantal personen in gezin moet een getal zijn.',
				'User' : account,
				'email2' : email2
			})
	
		checkrecord = User.objects.filter(email_text = email1)
		
		if (checkrecord.count() > 0):
			return render(request, 'register.html', {
				'title' : 'Registreren',
				'result' : 0,
				'result_msg' : 'Dit e-mailadres is al in gebruik.',
				'User' : account,
				'email2' : email2
			})
		else:
			try:
				account.save()
				return render(request, 'register.html', {
					'title' : 'Registreren',
					'result' : 1,
					'result_msg' : 'Uw account is met succes aangemaakt. U wordt doorverwezen.'
				})
			except Exception as e:
				return HttpResponse(e.message)
	else:
		return render(request, 'register.html', {
			'title' : 'Registreren'
		})
	
def account(request):
	UserAccountId = request.session.get('UserAccountId', None)
	provider = request.session.get('provider', None)
	
	if UserAccountId is not None:
		account = User.objects.get(id=UserAccountId)
		
		if request.method == 'GET':
			return render(request, 'account.html', {
				'title' : 'Mijn account',
				'User' : account,
				'provider' : provider
			})
		if request.method == 'POST':
			name = request.POST.get('name', '')
			address = request.POST.get('address', '')
			zipcode = request.POST.get('zipcode', '')
			city = request.POST.get('city', '')
			phone = request.POST.get('phone', '')
			famsize = request.POST.get('famsize', '')
			
			account = User(name_text = name, 
					email_text = account.email_text,
					address_text = address, 
					zipcode_text = zipcode, 
					city_text = city,
					phone_text = phone,
					familysize = famsize)
			
			if (name in [None, '']):
				return render(request, 'account.html', {
					'title' : 'Mijn account',
					'result' : 0,
					'result_msg' : 'Naam mag niet leeg zijn.',
					'User' : account,
					'provider' : provider
				})
			if (zipcode in [None, '']):
				return render(request, 'account.html', {
					'title' : 'Mijn account',
					'result' : 0,
					'result_msg' : 'Postcode mag niet leeg zijn.',
					'User' : account,
					'provider' : provider
				})
			if (len(zipcode) < 4 or zipcode[:4].isdigit() != True):
				return render(request, 'account.html', {
					'title' : 'Mijn account',
					'result' : 0,
					'result_msg' : 'Postcode moet minimaal uit 4 getallen bestaan.',
					'User' : account,
					'provider' : provider
				})
			if (famsize in [None, '']):
				return render(request, 'account.html', {
					'title' : 'Mijn account',
					'result' : 0,
					'result_msg' : 'Aantal personen in gezin mag niet leeg zijn.',
					'User' : account,
					'provider' : provider
				})
			if (famsize.isdigit() != True):
				return render(request, 'account.html', {
					'title' : 'Mijn account',
					'result' : 0,
					'result_msg' : 'Aantal personen in gezin moet een getal zijn.',
					'User':account,
					'provider' : provider
				})
			
			try: 
				User.objects.filter(id=UserAccountId).update(name_text = name, address_text = address, zipcode_text = zipcode, city_text = city, phone_text = phone, familysize = famsize)
				account = User.objects.get(id=UserAccountId)
				return render(request, 'account.html', {
					'title' : 'Mijn account',
					'result' : 1,
					'result_msg' : 'Uw account is met succes aangepast.',
					'User' : account,
					'provider' : provider
				})
			except Exception as e:
				return HttpResponse(e.message)
	else:
		return HttpResponseRedirect('/login?referer=account')

def password(request):
	UserAccountId = request.session.get('UserAccountId', None)
	provider = request.session.get('provider', None)
	
	if UserAccountId is not None:
		account = User.objects.get(id=UserAccountId)
		
		if request.method == 'GET':
			return render(request, 'password.html', {
				'title' : 'Wachtwoord wijzigen',
				'User' : account,
				'provider' : provider
			})
		if request.method == 'POST':
			password1 = request.POST.get('password1', '')
			password2 = request.POST.get('password2', '')
			userpass = str(hashlib.md5(password1).hexdigest())
			
			if (password1 in [None, '']):
				return render(request, 'password.html', {
					'title' : 'Wachtwoord wijzigen',
					'result' : 0,
					'result_msg' : 'Wachtwoord mag niet leeg zijn.', 
					'User' : account,
					'provider' : provider
				})
			if (password1 != password2):
				return render(request, 'password.html', {
					'title' : 'Wachtwoord wijzigen',
					'result' : 0,
					'result_msg' : 'De wachtwoorden komen niet overeen.', 
					'User' : account,
					'provider' : provider
				})
			
			try: 
				User.objects.filter(id=UserAccountId).update(password = userpass)
				return render(request, 'password.html', {
					'title' : 'Wachtwoord wijzigen',
					'result' : 1,
					'result_msg' : 'Uw wachtwoord is met succes gewijzigd.',
					'User' : account,
					'provider' : provider
				})
			except Exception as e:
				return HttpResponse(e.message)
	else:
		return HttpResponseRedirect('/login?referer=password')

def appliances(request):
	UserAccountId = request.session.get('UserAccountId', None)
	provider = request.session.get('provider', None)
	
	if UserAccountId is not None:
		account = User.objects.get(id=UserAccountId)
		appliances = Appliances.objects.filter(user=account).order_by('brand_text', 'type_nr')
		
		return render(request, 'appliances.html', {
			'title' : 'Apparaten',
			'User' : account,
			'provider' : provider,
			'Appliances' : appliances
		})
	else:
		return HttpResponseRedirect('/login?referer=appliances')

def productions(request):
	UserAccountId = request.session.get('UserAccountId', None)
	provider = request.session.get('provider', None)
	
	if UserAccountId is not None:
		account = User.objects.select_related().get(id=UserAccountId)
		productions = Productions.objects.filter(appliance__user=account)
		
		return render(request, 'productions.html', {
			'title' : 'Opbrengsten',
			'User' : account,
			'provider' : provider,
			'Productions' : productions
		})
	else:
		return HttpResponseRedirect('/login?referer=productions')

def graphs(request):
	UserAccount = request.session.get('UserAccountId', None)
	provider = request.session.get('provider', None)

	if UserAccount is not None:
		account = User.objects.get(id=UserAccount)
		return render(request, 'graphs.html', {
			'title' : 'Grafieken', 
			'User' : account,
			'provider' : provider
		})
	else:
		return HttpResponseRedirect('/login?referer=graphs')
