from django.http import HttpResponseRedirect
from django.shortcuts import render
from opdracht.power.utils.tools import getKwList, getKwTypeList
from opdracht.power.models import User, Appliances

def grafiek(request):
	app_id = request.GET.get('id')
	UserAccountId = request.session.get('UserAccountId', None)
	provider = request.session.get('provider', None)
	
	if UserAccountId is not None:
		account = User.objects.get(id=UserAccountId)
		
		if app_id in [None, ''] :
			return render(request, 'appliance/appliance_edit.html', {
				'title' : 'Grafiek',
				'result' : 0,
				'result_msg' : 'U heeft geen apparaat geselecteerd.',
				'User' : account,
				'provider' : provider
			})
		
		try:
			appliance = Appliances.objects.get(id=app_id)
		except:
			return render(request, 'graph/grafiek.html', {
				'title' : 'Grafiek',
				'result' : 0,
				'result_msg' : 'Er bestaat geen apparaat met id: ' + app_id,
				'User' : account,
				'provider' : provider
			})
			
		if appliance.user.id != UserAccountId :
			return render(request, 'graph/grafiek.html', {
				'title' : 'Grafiek',
				'result' : 0,
				'result_msg' : 'Dit apparaat is niet van u.',
				'User' : account,
				'provider' : provider
			})
			
		prod_eigen = getKwList(appliance)
		prod_gem = getKwTypeList(appliance.type_nr)
		return render(request, 'graph/grafiek.html', {
			'title' : 'Grafiek',
			'User' : account,
			'provider' : provider, 
			'Appliance' : appliance,
			'prod_eigen' : prod_eigen,
			'prod_gem' : prod_gem,
			'app_id' : app_id
		})
	else:
		return HttpResponseRedirect('/login?referer=grafiek?id=' + app_id)
