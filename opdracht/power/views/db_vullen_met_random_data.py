import random
from django.http import HttpResponseRedirect
from django.shortcuts import render
from opdracht.power.utils import tools
from opdracht.power.models import User, Appliances, Productions

def vulDatabaseMetRandomData(aantal):
    i = 0
    while i < aantal:
        """
        Tabel power_user
        """
        voornaam = tools.getRandomVoornaam()
        achternaam = tools.getRandomAchternaam()
        naam = tools.getNaam(voornaam, achternaam)
        domein = tools.getRandomDomein()
        email = tools.getEmail(voornaam, achternaam, domein)
        wachtwoord = tools.getWachtwoord(voornaam)
        postcode = tools.getRandomZipcode()
        famsize = tools.getRandomPersonenInHuishouden()
        
        account = User(name_text = naam, 
                       email_text = email,
                       password = wachtwoord, 
                       zipcode_text = postcode, 
                       familysize = famsize)
        print "i: " + str(i)
        checkrecord = User.objects.filter(email_text = email)
        if checkrecord.count() > 0:
            print "i: " + str(i) + " - e-mailadres: " + email + " bestaat al."
            continue
        
        account.save()
        
        j = 0
        aantal_apparaten = random.randint(1, 8)
        while j < aantal_apparaten:
            """
            Tabel power_appliances
            """
            soort = tools.getRandomSoort()
            lokatie = tools.getRandomLokatie()
            omschrijving = tools.getOmschrijving(soort, lokatie)
            merk = tools.getRandomMerk(soort)
            typenummer = tools.getTypenummer(merk)
            
            appliances = Appliances(soort_text = soort,
                                    desc_text = omschrijving, 
                                    brand_text = merk, 
                                    type_nr = typenummer,
                                    user = account)
            appliances.save()
            """
            Tabel power_productions
            """
            productie_kw_0900 = tools.getRandomkW(soort)
            productie_kw_1000 = tools.getRandomkW(soort)
            productie_kw_1100 = tools.getRandomkW(soort)
            productie_kw_1200 = tools.getRandomkW(soort)
            productie_kw_1300 = tools.getRandomkW(soort)
            productie_kw_1400 = tools.getRandomkW(soort)
            productie_kw_1500 = tools.getRandomkW(soort)
            productie_kw_1600 = tools.getRandomkW(soort)
            productie_kw_1700 = tools.getRandomkW(soort)
            productie_kw_1800 = tools.getRandomkW(soort)
            
            productie_kwh_0900 = productie_kw_0900
            productie_kwh_1000 = productie_kwh_0900 + productie_kw_1000
            productie_kwh_1100 = productie_kwh_1000 + productie_kw_1100
            productie_kwh_1200 = productie_kwh_1100 + productie_kw_1200
            productie_kwh_1300 = productie_kwh_1200 + productie_kw_1300
            productie_kwh_1400 = productie_kwh_1300 + productie_kw_1400
            productie_kwh_1500 = productie_kwh_1400 + productie_kw_1500
            productie_kwh_1600 = productie_kwh_1500 + productie_kw_1600
            productie_kwh_1700 = productie_kwh_1600 + productie_kw_1700
            productie_kwh_1800 = productie_kwh_1700 + productie_kw_1800
            
            production = Productions(production_kw_0900 = productie_kw_0900, 
                                     production_kwh_0900 = productie_kwh_0900,
                                     production_kw_1000 = productie_kw_1000, 
                                     production_kwh_1000 = productie_kwh_1000, 
                                     production_kw_1100 = productie_kw_1100, 
                                     production_kwh_1100 = productie_kwh_1100, 
                                     production_kw_1200 = productie_kw_1200, 
                                     production_kwh_1200 = productie_kwh_1200, 
                                     production_kw_1300 = productie_kw_1300, 
                                     production_kwh_1300 = productie_kwh_1300,
                                     production_kw_1400 = productie_kw_1400, 
                                     production_kwh_1400 = productie_kwh_1400,
                                     production_kw_1500 = productie_kw_1500, 
                                     production_kwh_1500 = productie_kwh_1500,
                                     production_kw_1600 = productie_kw_1600, 
                                     production_kwh_1600 = productie_kwh_1600,
                                     production_kw_1700 = productie_kw_1700, 
                                     production_kwh_1700 = productie_kwh_1700,
                                     production_kw_1800 = productie_kw_1800, 
                                     production_kwh_1800 = productie_kwh_1800,  
                                     appliance = appliances)
            production.save()
            j += 1
            
        i += 1

def vulDatabase(request):
    UserAccountId = request.session.get('UserAccountId', None)
    provider = request.session.get('provider', None)
    
    if UserAccountId is not None:
        account = User.objects.get(id=UserAccountId)

        if account.email_text != "wesley.kappen@gmail.com" and account.email_text != "pieterkuiper@gmail.com":
            return render(request, 'db_vullen_met_random_data.html', {
                    'title' : 'Database vullen met random data',
                    'result' : 0,
                    'result_msg' : 'Verboden toegang!',
                    'verboden' : True,
                    'User' : account,
                    'provider' : provider
                })

        if request.method == 'POST':
            aantal = request.POST.get('aantal', '')

            if (aantal in [None, '']):
                return render(request, 'db_vullen_met_random_data.html', {
                    'title' : 'Database vullen met random data',
                    'result' : 0,
                    'result_msg' : 'Aantal users mag niet leeg zijn.',
                    'verboden' : False,
                    'User' : account,
                    'provider' : provider,
                    'aantal' : aantal
                })
            if (aantal.isdigit() != True):
                return render(request, 'db_vullen_met_random_data.html', {
                    'title' : 'Database vullen met random data',
                    'result' : 0,
                    'result_msg' : 'Aantal users moet een getal zijn.',
                    'verboden' : False,
                    'User' : account,
                    'provider' : provider,
                    'aantal' : aantal
                })
            try:
                vulDatabaseMetRandomData(int(aantal))
                return render(request, 'db_vullen_met_random_data.html', {
                        'title' : 'Database vullen met random data', 
                        'result' : 1,
                        'result_msg' : 'Er is met succes random data aan de database toegevoegd.',
                        'verboden' : False,
                        'User' : account,
                        'provider' : provider,
                        'aantal' : aantal
                    })
            except Exception:
                return render(request, 'db_vullen_met_random_data.html', {
                        'title' : 'Database vullen met random data', 
                        'result' : 0,
                        'result_msg' : 'Er is iets fout gegaan.',
                        'verboden' : False,
                        'User' : account,
                        'provider' : provider,
                        'aantal' : aantal
                    })
        else:
            return render(request, 'db_vullen_met_random_data.html', {
                'title' : 'Database vullen met random data',
                'verboden' : False,
                'User' : account,
                'provider' : provider
            })
    else:
        return HttpResponseRedirect('/login?referer=db_vullen_met_random_data')