from django.conf.urls import patterns, url

from opdracht.power.views import main, appliance, production, db_vullen_met_random_data, graph, facebook, google
from opdracht.power.api import rest

urlpatterns = patterns('',
    url(r'^$', main.index),
    url(r'^register', main.register),
    url(r'^login', main.login),
    url(r'^logout', main.logout),
    url(r'^account', main.account),
    url(r'^password', main.password),
    url(r'^appliances', main.appliances),
    url(r'^productions', main.productions),
    url(r'^graphs', main.graphs),
    url(r'^privacy-policy', main.privacypolicy),
    
    url(r'^appliance/add', appliance.appliance_add),
    url(r'^appliance/edit', appliance.appliance_edit),
    
    url(r'^production/add', production.production_add),
    
    url(r'^graph/grafiek', graph.grafiek),
    
    url(r'^db_vullen_met_random_data', db_vullen_met_random_data.vulDatabase),
    
    url(r'^api/appliances/get', rest.getAppliances),
    url(r'^api/productions/get', rest.getProductions),
    
    url(r'^facebook', facebook.facebook),
    url(r'^google', google.google),
)