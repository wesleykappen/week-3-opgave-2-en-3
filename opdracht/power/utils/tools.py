import random, hashlib
from django.db.models import Avg
from opdracht.power.models import Productions

def getProductionValues(appliances):
    app = dict()

    for key_app in appliances:
        productions = Productions.objects.filter(appliance = key_app).order_by('appliance', 'production_time')
        dict_kw = dict()
        dict_kwh = dict()

        for key_prod in productions:
            dict_kw[str(key_prod.production_time).replace(':', '')] = key_prod.production_kw
            dict_kwh[str(key_prod.production_time).replace(':', '')] = key_prod.production_kwh

        dict_prod = dict(kw = dict_kw, kwh = dict_kwh)
        app[key_app.desc_text + ' (' + key_app.brand_text + ')'] = dict_prod

    return app

def getKwList(appliance):
    productions = None
    try:
        productions = Productions.objects.get(appliance = appliance)
    except Exception:
        productions = Productions(production_kw_0900 = 0,  
                    production_kw_1000 = 0,
                    production_kw_1100 = 0,
                    production_kw_1200 = 0,
                    production_kw_1300 = 0,
                    production_kw_1400 = 0,
                    production_kw_1500 = 0, 
                    production_kw_1600 = 0, 
                    production_kw_1700 = 0, 
                    production_kw_1800 = 0,  
                    appliance = appliance)
    
    prod_list = []
    prod_list.append(productions.production_kw_0900)
    prod_list.append(productions.production_kw_1000)
    prod_list.append(productions.production_kw_1100)
    prod_list.append(productions.production_kw_1200)
    prod_list.append(productions.production_kw_1300)
    prod_list.append(productions.production_kw_1400)
    prod_list.append(productions.production_kw_1500)
    prod_list.append(productions.production_kw_1600)
    prod_list.append(productions.production_kw_1700)
    prod_list.append(productions.production_kw_1800)
    
    return prod_list

def getKwTypeList(type_nr):
    productions = Productions.objects.filter(appliance__type_nr = type_nr).aggregate(production_kw_0900=Avg('production_kw_0900'),
                                                                                     production_kw_1000=Avg('production_kw_1000'),
                                                                                     production_kw_1100=Avg('production_kw_1100'),
                                                                                     production_kw_1200=Avg('production_kw_1200'),
                                                                                     production_kw_1300=Avg('production_kw_1300'),
                                                                                     production_kw_1400=Avg('production_kw_1400'),
                                                                                     production_kw_1500=Avg('production_kw_1500'),
                                                                                     production_kw_1600=Avg('production_kw_1600'),
                                                                                     production_kw_1700=Avg('production_kw_1700'),
                                                                                     production_kw_1800=Avg('production_kw_1800'))
    
    if productions.get('production_kw_0900') in [None, '']:
        prod_list = []
        prod_list.append(0)
        prod_list.append(0)
        prod_list.append(0)
        prod_list.append(0)
        prod_list.append(0)
        prod_list.append(0)
        prod_list.append(0)
        prod_list.append(0)
        prod_list.append(0)
        prod_list.append(0)
        
        return prod_list
        
    
    prod_list = []
    prod_list.append(round(productions.get('production_kw_0900'), 2))
    prod_list.append(round(productions.get('production_kw_1000'), 2))
    prod_list.append(round(productions.get('production_kw_1100'), 2))
    prod_list.append(round(productions.get('production_kw_1200'), 2))
    prod_list.append(round(productions.get('production_kw_1300'), 2))
    prod_list.append(round(productions.get('production_kw_1400'), 2))
    prod_list.append(round(productions.get('production_kw_1500'), 2))
    prod_list.append(round(productions.get('production_kw_1600'), 2))
    prod_list.append(round(productions.get('production_kw_1700'), 2))
    prod_list.append(round(productions.get('production_kw_1800'), 2))
    
    return prod_list

def getRandomVoornaam():
    i = random.randint(0, 25)
    if i == 0:
        voornaam = "Daan"
    elif i == 1:
        voornaam = "Sem"
    elif i == 2:
        voornaam = "Milan"
    elif i == 3:
        voornaam = "Levi"
    elif i == 4:
        voornaam = "Luuk"
    elif i == 5:
        voornaam = "Emma"
    elif i == 6:
        voornaam = "Julia"
    elif i == 7:
        voornaam = "Sophie"
    elif i == 8:
        voornaam = "Lotte"
    elif i == 9:
        voornaam = "Isa"
    elif i == 10:
        voornaam = "Pieter"
    elif i == 11:
        voornaam = "Wesley"
    elif i == 12:
        voornaam = "Bart"
    elif i == 13:
        voornaam = "Henkjan"
    elif i == 14:
        voornaam = "Aaf"
    elif i == 15:
        voornaam = "Debbie"
    elif i == 16:
        voornaam = "Elise"
    elif i == 17:
        voornaam = "Ted"
    elif i == 18:
        voornaam = "Vincent"
    elif i == 19:
        voornaam = "Youssef"
    elif i == 20:
        voornaam = "Toon"
    elif i == 21:
        voornaam = "Viola"
    elif i == 22:
        voornaam = "Tea"
    elif i == 23:
        voornaam = "Saar"
    elif i == 24:
        voornaam = "Ruth"
    else:
        voornaam = "Chantal"
    
    return voornaam

def getRandomAchternaam():
    i = random.randint(0, 25)
    if i == 0:
        achternaam = "de Jong"
    elif i == 1:
        achternaam = "de Vries"
    elif i == 2:
        achternaam = "Jansen"
    elif i == 3:
        achternaam = "van der Berg"
    elif i == 4:
        achternaam = "van den Berg"
    elif i == 5:
        achternaam = "van de Berg"
    elif i == 6:
        achternaam = "Bakker"
    elif i == 7:
        achternaam = "van Dijk"
    elif i == 8:
        achternaam = "Visser"
    elif i == 9:
        achternaam = "Janssen"
    elif i == 10:
        achternaam = "Smit"
    elif i == 11:
        achternaam = "Meijer"
    elif i == 12:
        achternaam = "Meyer"
    elif i == 13:
        achternaam = "Kuiper"
    elif i == 14:
        achternaam = "Kappen"
    elif i == 15:
        achternaam = "Barnard"
    elif i == 16:
        achternaam = "Hekman"
    elif i == 17:
        achternaam = "Best"
    elif i == 18:
        achternaam = "Dijkstra"
    elif i == 19:
        achternaam = "Hakker"
    elif i == 20:
        achternaam = "Holman"
    elif i == 21:
        achternaam = "Jonkers"
    elif i == 22:
        achternaam = "Kranenburg"
    elif i == 23:
        achternaam = "Molenaar"
    elif i == 24:
        achternaam = "Peters"
    else:
        achternaam = "Buist"
    
    return achternaam

def getRandomDomein():
    i = random.randint(0, 12)
    if i == 0:
        domein = "st.hanze.nl"
    elif i == 1:
        domein = "hotmail.com"
    elif i == 2:
        domein = "gmail.com"
    elif i == 3:
        domein = "yahoo.com"
    elif i == 4:
        domein = "kpnplanet.nl"
    elif i == 5:
        domein = "ziggo.nl"
    elif i == 6:
        domein = "xs4all.nl"
    elif i == 7:
        domein = "tele2.nl"
    elif i == 8:
        domein = "home.nl"
    elif i == 9:
        domein = "online.nl"
    elif i == 10:
        domein = "pl.hanze.nl"
    elif i == 11:
        domein = "outlook.com"
    else:
        domein = "telfort.nl"
    
    return domein

def getRandomSoort():
    i = random.randint(0, 1)
    if i == 0:
        soort = "Windmolen"
    else:
        soort = "Zonnepaneel"

    return soort

def getRandomLokatie():
    i = random.randint(0, 4)
    if i == 0:
        lokatie = "op het dak voor"
    elif i == 1:
        lokatie = "op het dak achter"
    elif i == 2:
        lokatie = "in de voortuin"
    elif i == 3:
        lokatie = "in de achtertuin"
    else:
        lokatie = "in het park"
    
    return lokatie  

def getRandomMerk(soort):
    i = random.randint(0, 5)
    if soort == "Zonnepaneel":
        if i == 0:
            merk = "ET Solar"
        elif i == 1:
            merk = "Mitsubishi"
        elif i == 2:
            merk = "Sanyo"
        elif i == 3:
            merk = "Sharp"
        elif i == 4:
            merk = "Solarwatt"
        else:
            merk = "SolarWorld"
    else:
        if i == 0:
            merk = "Aircon"
        elif i == 1:
            merk = "Sirocco"
        elif i == 2:
            merk = "MARC"
        elif i == 3:
            merk = "SkyStream"
        elif i == 4:
            merk = "Fortis"
        else:
            merk = "PAM"
    
    return merk

def getTypenummer(merk):
    i = random.randint(0, 1)
    if merk == "ET Solar":
        if i == 0:
            typenummer = "E-5"
        else:
            typenummer = "E-10"
    elif merk == "Mitsubishi":
        if i == 0:
            typenummer = "M-4"
        else:
            typenummer = "M-8"
    elif merk == "Sanyo":
        if i == 0:
            typenummer = "SA-18"
        else:
            typenummer = "SA-22"
    elif merk == "Sharp":
        if i == 0:
            typenummer = "SH-9"
        else:
            typenummer = "SH-18"
    elif merk == "Solarwatt":
        if i == 0:
            typenummer = "S-1"
        else:
            typenummer = "S-2"
    elif merk == "SolarWorld":
        if i == 0:
            typenummer = "SW-72"
        else:
            typenummer = "SW-112"
    elif merk == "Aircon":
        if i == 0:
            typenummer = "A-500"
        else:
            typenummer = "A-1000"
    elif merk == "Sirocco":
        if i == 0:
            typenummer = "SC-288"
        else:
            typenummer = "SC-700"
    elif merk == "MARC":
        if i == 0:
            typenummer = "MARC-900"
        else:
            typenummer = "MARC-1800"
    elif merk == "SkyStream":
        if i == 0:
            typenummer = "SS-77"
        else:
            typenummer = "SS-144"
    else:
        if i == 0:
            typenummer = "PAM-444"
        else:
            typenummer = "PAM-888"
            
    
    return typenummer

def getRandomPersonenInHuishouden():
    return random.randint(1, 6)

def getNaam(voornaam, achternaam):
    return voornaam + " " + achternaam

def getEmail(voornaam, achternaam, domein):
    return voornaam.lower() + "." + achternaam.lower().replace(" ", ".") + "@" + domein

def getRandomZipcode():
    return random.randint(9711, 9750)

def getWachtwoord(voornaam):
    return str(hashlib.md5(voornaam.lower()).hexdigest())

def getOmschrijving(soort, lokatie):
    return soort + " " + lokatie

def getRandomkW(soort):
    if soort == "Zonnepaneel":
        productie_kw = round(random.uniform(0.01, 2.00), 2)
    else:
        productie_kw = round(random.uniform(1.00, 10.00), 2)
    
    return productie_kw

def getProductieTijden():
    productie_tijden = ["09:00", "10:00", "11:00", "12:00", "13:00", "14:00", "15:00", "16:00", "17:00", "18:00"]
    
    return productie_tijden
